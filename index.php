<?php
include_once 'inc/db.php';
include_once 'inc/functions.php';
 
sec_session_start();
 
if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Sign In</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css" />
       
        <script type="text/JavaScript" src="js/sha512.js"></script>
    	<script type="text/JavaScript" src="js/scripts.js"></script> 	    
    </head>
    <body>
    	<div class="login-container">	
			<?php if (login_check($mysqli) == true) {?>
            <div class="form-signin">
            	<div class="alert alert-success text-center" role="alert">You are logged as: <?php echo htmlentities($_SESSION['username']);?>
                	<p><a href="inc/logout.php">Log out</a></p>
                </div>
            </div>    
            <?php     
            } else {
            ?> 
            <form class="form-signin" action="inc/login-action.php" method="post" name="login_form">   
                <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1> 
               	<?php
					if (isset($_GET['error'])) {?>
					<div class="alert alert-warning text-center" role="alert"><strong>Error</strong>: There was an error while logging you in. Please check your email or password and try again.</div>
				<?php }?>             
                <label for="inputEmail" class="sr-only">Email: </label>
                <input name="email" type="text" id="inputEmail" class="form-control" placeholder="Email" required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit" onclick="formhash(this.form, this.form.password);" />Sign in</button>
            </form>
        </div>
    	<?php } ?> 	    
    </body>
</html>