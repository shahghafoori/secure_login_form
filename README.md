
## Secure PHP Login Form

Secure Login form created by shah Ghafoori for Yes Life Cycle Marketing. Includes the following security Features:

1. SQL Injections
2. Session Hijacking
3. Network Eavesdropping
4. Cross Site Scripting
5. Brute Force Attacks
6. Covert Timing Channel Attacks