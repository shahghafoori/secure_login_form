<?php
include_once 'db.php';
include_once 'functions.php';
sec_session_start(); // Store Sessions
 
if (isset($_POST)){
	$email = $_POST['email'];
	if(isset($_POST['p'])){
		$password = $_POST['p']; // If JavaScript is enabled. Get Hash Value
	}
	else{
		$password = hash('sha512', $_POST['password']); // If JavaScript is disabled
	}
	if (login($email, $password, $mysqli) == true) {
        // Login success 
        header('Location: ../welcome.php');
    } else {
        // Login failed 
        header('Location: ../index.php?error=1');
    }
	} else {
    // The correct POST variables were not sent to this page. 
    echo 'Invalid Request';
}