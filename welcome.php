<?php
include_once 'inc/db.php';
include_once 'inc/functions.php';
 
sec_session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Welcome</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css" />
    </head>
    <body>
    	<div class="login-container">
        <?php if (login_check($mysqli) == true) : ?>
            <div class="alert alert-success text-center alert-container "role="alert"><strong>Welcome to our function:</strong> You are logged as: <?php echo htmlentities($_SESSION['username']);?>
                	<p><a href="inc/logout.php">Log out</a></p>
                </div>
            
        <?php else : ?>
            <p>
                <span class="error">You are not authorized to access this page.</span> Please <a href="index.php">login</a>.
            </p>
        <?php endif; ?>
        </div>
    </body>
</html>